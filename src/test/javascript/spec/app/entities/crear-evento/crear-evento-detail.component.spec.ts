import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppEventosOscarTestModule } from '../../../test.module';
import { CrearEventoDetailComponent } from 'app/entities/crear-evento/crear-evento-detail.component';
import { CrearEvento } from 'app/shared/model/crear-evento.model';

describe('Component Tests', () => {
  describe('CrearEvento Management Detail Component', () => {
    let comp: CrearEventoDetailComponent;
    let fixture: ComponentFixture<CrearEventoDetailComponent>;
    const route = ({ data: of({ crearEvento: new CrearEvento(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppEventosOscarTestModule],
        declarations: [CrearEventoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CrearEventoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CrearEventoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load crearEvento on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.crearEvento).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
