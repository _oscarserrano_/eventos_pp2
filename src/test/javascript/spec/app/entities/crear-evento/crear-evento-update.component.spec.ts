import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AppEventosOscarTestModule } from '../../../test.module';
import { CrearEventoUpdateComponent } from 'app/entities/crear-evento/crear-evento-update.component';
import { CrearEventoService } from 'app/entities/crear-evento/crear-evento.service';
import { CrearEvento } from 'app/shared/model/crear-evento.model';

describe('Component Tests', () => {
  describe('CrearEvento Management Update Component', () => {
    let comp: CrearEventoUpdateComponent;
    let fixture: ComponentFixture<CrearEventoUpdateComponent>;
    let service: CrearEventoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppEventosOscarTestModule],
        declarations: [CrearEventoUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CrearEventoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CrearEventoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CrearEventoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CrearEvento(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CrearEvento();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
