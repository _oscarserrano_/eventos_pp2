import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { CrearEventoService } from 'app/entities/crear-evento/crear-evento.service';
import { ICrearEvento, CrearEvento } from 'app/shared/model/crear-evento.model';

describe('Service Tests', () => {
  describe('CrearEvento Service', () => {
    let injector: TestBed;
    let service: CrearEventoService;
    let httpMock: HttpTestingController;
    let elemDefault: ICrearEvento;
    let expectedResult: ICrearEvento | ICrearEvento[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CrearEventoService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new CrearEvento(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, 0, false, 0, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            fechaHora: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CrearEvento', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            fechaHora: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fechaHora: currentDate,
          },
          returnedFromService
        );

        service.create(new CrearEvento()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CrearEvento', () => {
        const returnedFromService = Object.assign(
          {
            nombreEvento: 'BBBBBB',
            informacion: 'BBBBBB',
            lugar: 'BBBBBB',
            fechaHora: currentDate.format(DATE_TIME_FORMAT),
            usuarioId: 1,
            tipoEvento: true,
            cantiadBoletas: 1,
            categoriaEvento: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fechaHora: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CrearEvento', () => {
        const returnedFromService = Object.assign(
          {
            nombreEvento: 'BBBBBB',
            informacion: 'BBBBBB',
            lugar: 'BBBBBB',
            fechaHora: currentDate.format(DATE_TIME_FORMAT),
            usuarioId: 1,
            tipoEvento: true,
            cantiadBoletas: 1,
            categoriaEvento: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fechaHora: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CrearEvento', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
