package com.usco.project.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AsistenciaMapperTest {

    private AsistenciaMapper asistenciaMapper;

    @BeforeEach
    public void setUp() {
        asistenciaMapper = new AsistenciaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(asistenciaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(asistenciaMapper.fromId(null)).isNull();
    }
}
