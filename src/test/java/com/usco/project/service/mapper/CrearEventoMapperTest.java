package com.usco.project.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CrearEventoMapperTest {

    private CrearEventoMapper crearEventoMapper;

    @BeforeEach
    public void setUp() {
        crearEventoMapper = new CrearEventoMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(crearEventoMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(crearEventoMapper.fromId(null)).isNull();
    }
}
