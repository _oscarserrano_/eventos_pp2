package com.usco.project.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.usco.project.web.rest.TestUtil;

public class CrearEventoDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CrearEventoDTO.class);
        CrearEventoDTO crearEventoDTO1 = new CrearEventoDTO();
        crearEventoDTO1.setId(1L);
        CrearEventoDTO crearEventoDTO2 = new CrearEventoDTO();
        assertThat(crearEventoDTO1).isNotEqualTo(crearEventoDTO2);
        crearEventoDTO2.setId(crearEventoDTO1.getId());
        assertThat(crearEventoDTO1).isEqualTo(crearEventoDTO2);
        crearEventoDTO2.setId(2L);
        assertThat(crearEventoDTO1).isNotEqualTo(crearEventoDTO2);
        crearEventoDTO1.setId(null);
        assertThat(crearEventoDTO1).isNotEqualTo(crearEventoDTO2);
    }
}
