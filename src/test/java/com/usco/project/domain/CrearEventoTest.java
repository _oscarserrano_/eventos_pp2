package com.usco.project.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.usco.project.web.rest.TestUtil;

public class CrearEventoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CrearEvento.class);
        CrearEvento crearEvento1 = new CrearEvento();
        crearEvento1.setId(1L);
        CrearEvento crearEvento2 = new CrearEvento();
        crearEvento2.setId(crearEvento1.getId());
        assertThat(crearEvento1).isEqualTo(crearEvento2);
        crearEvento2.setId(2L);
        assertThat(crearEvento1).isNotEqualTo(crearEvento2);
        crearEvento1.setId(null);
        assertThat(crearEvento1).isNotEqualTo(crearEvento2);
    }
}
