package com.usco.project.web.rest;

import com.usco.project.AppEventosOscarApp;
import com.usco.project.domain.Usuario;
import com.usco.project.repository.UsuarioRepository;
import com.usco.project.service.UsuarioService;
import com.usco.project.service.dto.UsuarioDTO;
import com.usco.project.service.mapper.UsuarioMapper;
import com.usco.project.service.dto.UsuarioCriteria;
import com.usco.project.service.UsuarioQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UsuarioResource} REST controller.
 */
@SpringBootTest(classes = AppEventosOscarApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class UsuarioResourceIT {

    private static final String DEFAULT_LOGIN_USUARIO = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN_USUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_NOMBRE_USUARIO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_USUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_FOTO = "AAAAAAAAAA";
    private static final String UPDATED_FOTO = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;
    private static final Long SMALLER_USER_ID = 1L - 1L;

    private static final Long DEFAULT_TIPO_DOCUMENTO = 1L;
    private static final Long UPDATED_TIPO_DOCUMENTO = 2L;
    private static final Long SMALLER_TIPO_DOCUMENTO = 1L - 1L;

    private static final Long DEFAULT_NUMERO_DOCUMENTO = 1L;
    private static final Long UPDATED_NUMERO_DOCUMENTO = 2L;
    private static final Long SMALLER_NUMERO_DOCUMENTO = 1L - 1L;

    private static final Long DEFAULT_TELEFONO = 1L;
    private static final Long UPDATED_TELEFONO = 2L;
    private static final Long SMALLER_TELEFONO = 1L - 1L;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioMapper usuarioMapper;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private UsuarioQueryService usuarioQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUsuarioMockMvc;

    private Usuario usuario;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Usuario createEntity(EntityManager em) {
        Usuario usuario = new Usuario()
            .loginUsuario(DEFAULT_LOGIN_USUARIO)
            .nombreUsuario(DEFAULT_NOMBRE_USUARIO)
            .foto(DEFAULT_FOTO)
            .userId(DEFAULT_USER_ID)
            .tipoDocumento(DEFAULT_TIPO_DOCUMENTO)
            .numeroDocumento(DEFAULT_NUMERO_DOCUMENTO)
            .telefono(DEFAULT_TELEFONO);
        return usuario;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Usuario createUpdatedEntity(EntityManager em) {
        Usuario usuario = new Usuario()
            .loginUsuario(UPDATED_LOGIN_USUARIO)
            .nombreUsuario(UPDATED_NOMBRE_USUARIO)
            .foto(UPDATED_FOTO)
            .userId(UPDATED_USER_ID)
            .tipoDocumento(UPDATED_TIPO_DOCUMENTO)
            .numeroDocumento(UPDATED_NUMERO_DOCUMENTO)
            .telefono(UPDATED_TELEFONO);
        return usuario;
    }

    @BeforeEach
    public void initTest() {
        usuario = createEntity(em);
    }

    @Test
    @Transactional
    public void createUsuario() throws Exception {
        int databaseSizeBeforeCreate = usuarioRepository.findAll().size();
        // Create the Usuario
        UsuarioDTO usuarioDTO = usuarioMapper.toDto(usuario);
        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuarioDTO)))
            .andExpect(status().isCreated());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeCreate + 1);
        Usuario testUsuario = usuarioList.get(usuarioList.size() - 1);
        assertThat(testUsuario.getLoginUsuario()).isEqualTo(DEFAULT_LOGIN_USUARIO);
        assertThat(testUsuario.getNombreUsuario()).isEqualTo(DEFAULT_NOMBRE_USUARIO);
        assertThat(testUsuario.getFoto()).isEqualTo(DEFAULT_FOTO);
        assertThat(testUsuario.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testUsuario.getTipoDocumento()).isEqualTo(DEFAULT_TIPO_DOCUMENTO);
        assertThat(testUsuario.getNumeroDocumento()).isEqualTo(DEFAULT_NUMERO_DOCUMENTO);
        assertThat(testUsuario.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
    }

    @Test
    @Transactional
    public void createUsuarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = usuarioRepository.findAll().size();

        // Create the Usuario with an existing ID
        usuario.setId(1L);
        UsuarioDTO usuarioDTO = usuarioMapper.toDto(usuario);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuarioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUsuarios() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList
        restUsuarioMockMvc.perform(get("/api/usuarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].loginUsuario").value(hasItem(DEFAULT_LOGIN_USUARIO)))
            .andExpect(jsonPath("$.[*].nombreUsuario").value(hasItem(DEFAULT_NOMBRE_USUARIO)))
            .andExpect(jsonPath("$.[*].foto").value(hasItem(DEFAULT_FOTO)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].tipoDocumento").value(hasItem(DEFAULT_TIPO_DOCUMENTO.intValue())))
            .andExpect(jsonPath("$.[*].numeroDocumento").value(hasItem(DEFAULT_NUMERO_DOCUMENTO.intValue())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.intValue())));
    }
    
    @Test
    @Transactional
    public void getUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get the usuario
        restUsuarioMockMvc.perform(get("/api/usuarios/{id}", usuario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(usuario.getId().intValue()))
            .andExpect(jsonPath("$.loginUsuario").value(DEFAULT_LOGIN_USUARIO))
            .andExpect(jsonPath("$.nombreUsuario").value(DEFAULT_NOMBRE_USUARIO))
            .andExpect(jsonPath("$.foto").value(DEFAULT_FOTO))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.tipoDocumento").value(DEFAULT_TIPO_DOCUMENTO.intValue()))
            .andExpect(jsonPath("$.numeroDocumento").value(DEFAULT_NUMERO_DOCUMENTO.intValue()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.intValue()));
    }


    @Test
    @Transactional
    public void getUsuariosByIdFiltering() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        Long id = usuario.getId();

        defaultUsuarioShouldBeFound("id.equals=" + id);
        defaultUsuarioShouldNotBeFound("id.notEquals=" + id);

        defaultUsuarioShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUsuarioShouldNotBeFound("id.greaterThan=" + id);

        defaultUsuarioShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUsuarioShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllUsuariosByLoginUsuarioIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where loginUsuario equals to DEFAULT_LOGIN_USUARIO
        defaultUsuarioShouldBeFound("loginUsuario.equals=" + DEFAULT_LOGIN_USUARIO);

        // Get all the usuarioList where loginUsuario equals to UPDATED_LOGIN_USUARIO
        defaultUsuarioShouldNotBeFound("loginUsuario.equals=" + UPDATED_LOGIN_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByLoginUsuarioIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where loginUsuario not equals to DEFAULT_LOGIN_USUARIO
        defaultUsuarioShouldNotBeFound("loginUsuario.notEquals=" + DEFAULT_LOGIN_USUARIO);

        // Get all the usuarioList where loginUsuario not equals to UPDATED_LOGIN_USUARIO
        defaultUsuarioShouldBeFound("loginUsuario.notEquals=" + UPDATED_LOGIN_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByLoginUsuarioIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where loginUsuario in DEFAULT_LOGIN_USUARIO or UPDATED_LOGIN_USUARIO
        defaultUsuarioShouldBeFound("loginUsuario.in=" + DEFAULT_LOGIN_USUARIO + "," + UPDATED_LOGIN_USUARIO);

        // Get all the usuarioList where loginUsuario equals to UPDATED_LOGIN_USUARIO
        defaultUsuarioShouldNotBeFound("loginUsuario.in=" + UPDATED_LOGIN_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByLoginUsuarioIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where loginUsuario is not null
        defaultUsuarioShouldBeFound("loginUsuario.specified=true");

        // Get all the usuarioList where loginUsuario is null
        defaultUsuarioShouldNotBeFound("loginUsuario.specified=false");
    }
                @Test
    @Transactional
    public void getAllUsuariosByLoginUsuarioContainsSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where loginUsuario contains DEFAULT_LOGIN_USUARIO
        defaultUsuarioShouldBeFound("loginUsuario.contains=" + DEFAULT_LOGIN_USUARIO);

        // Get all the usuarioList where loginUsuario contains UPDATED_LOGIN_USUARIO
        defaultUsuarioShouldNotBeFound("loginUsuario.contains=" + UPDATED_LOGIN_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByLoginUsuarioNotContainsSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where loginUsuario does not contain DEFAULT_LOGIN_USUARIO
        defaultUsuarioShouldNotBeFound("loginUsuario.doesNotContain=" + DEFAULT_LOGIN_USUARIO);

        // Get all the usuarioList where loginUsuario does not contain UPDATED_LOGIN_USUARIO
        defaultUsuarioShouldBeFound("loginUsuario.doesNotContain=" + UPDATED_LOGIN_USUARIO);
    }


    @Test
    @Transactional
    public void getAllUsuariosByNombreUsuarioIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where nombreUsuario equals to DEFAULT_NOMBRE_USUARIO
        defaultUsuarioShouldBeFound("nombreUsuario.equals=" + DEFAULT_NOMBRE_USUARIO);

        // Get all the usuarioList where nombreUsuario equals to UPDATED_NOMBRE_USUARIO
        defaultUsuarioShouldNotBeFound("nombreUsuario.equals=" + UPDATED_NOMBRE_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNombreUsuarioIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where nombreUsuario not equals to DEFAULT_NOMBRE_USUARIO
        defaultUsuarioShouldNotBeFound("nombreUsuario.notEquals=" + DEFAULT_NOMBRE_USUARIO);

        // Get all the usuarioList where nombreUsuario not equals to UPDATED_NOMBRE_USUARIO
        defaultUsuarioShouldBeFound("nombreUsuario.notEquals=" + UPDATED_NOMBRE_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNombreUsuarioIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where nombreUsuario in DEFAULT_NOMBRE_USUARIO or UPDATED_NOMBRE_USUARIO
        defaultUsuarioShouldBeFound("nombreUsuario.in=" + DEFAULT_NOMBRE_USUARIO + "," + UPDATED_NOMBRE_USUARIO);

        // Get all the usuarioList where nombreUsuario equals to UPDATED_NOMBRE_USUARIO
        defaultUsuarioShouldNotBeFound("nombreUsuario.in=" + UPDATED_NOMBRE_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNombreUsuarioIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where nombreUsuario is not null
        defaultUsuarioShouldBeFound("nombreUsuario.specified=true");

        // Get all the usuarioList where nombreUsuario is null
        defaultUsuarioShouldNotBeFound("nombreUsuario.specified=false");
    }
                @Test
    @Transactional
    public void getAllUsuariosByNombreUsuarioContainsSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where nombreUsuario contains DEFAULT_NOMBRE_USUARIO
        defaultUsuarioShouldBeFound("nombreUsuario.contains=" + DEFAULT_NOMBRE_USUARIO);

        // Get all the usuarioList where nombreUsuario contains UPDATED_NOMBRE_USUARIO
        defaultUsuarioShouldNotBeFound("nombreUsuario.contains=" + UPDATED_NOMBRE_USUARIO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNombreUsuarioNotContainsSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where nombreUsuario does not contain DEFAULT_NOMBRE_USUARIO
        defaultUsuarioShouldNotBeFound("nombreUsuario.doesNotContain=" + DEFAULT_NOMBRE_USUARIO);

        // Get all the usuarioList where nombreUsuario does not contain UPDATED_NOMBRE_USUARIO
        defaultUsuarioShouldBeFound("nombreUsuario.doesNotContain=" + UPDATED_NOMBRE_USUARIO);
    }


    @Test
    @Transactional
    public void getAllUsuariosByFotoIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where foto equals to DEFAULT_FOTO
        defaultUsuarioShouldBeFound("foto.equals=" + DEFAULT_FOTO);

        // Get all the usuarioList where foto equals to UPDATED_FOTO
        defaultUsuarioShouldNotBeFound("foto.equals=" + UPDATED_FOTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByFotoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where foto not equals to DEFAULT_FOTO
        defaultUsuarioShouldNotBeFound("foto.notEquals=" + DEFAULT_FOTO);

        // Get all the usuarioList where foto not equals to UPDATED_FOTO
        defaultUsuarioShouldBeFound("foto.notEquals=" + UPDATED_FOTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByFotoIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where foto in DEFAULT_FOTO or UPDATED_FOTO
        defaultUsuarioShouldBeFound("foto.in=" + DEFAULT_FOTO + "," + UPDATED_FOTO);

        // Get all the usuarioList where foto equals to UPDATED_FOTO
        defaultUsuarioShouldNotBeFound("foto.in=" + UPDATED_FOTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByFotoIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where foto is not null
        defaultUsuarioShouldBeFound("foto.specified=true");

        // Get all the usuarioList where foto is null
        defaultUsuarioShouldNotBeFound("foto.specified=false");
    }
                @Test
    @Transactional
    public void getAllUsuariosByFotoContainsSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where foto contains DEFAULT_FOTO
        defaultUsuarioShouldBeFound("foto.contains=" + DEFAULT_FOTO);

        // Get all the usuarioList where foto contains UPDATED_FOTO
        defaultUsuarioShouldNotBeFound("foto.contains=" + UPDATED_FOTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByFotoNotContainsSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where foto does not contain DEFAULT_FOTO
        defaultUsuarioShouldNotBeFound("foto.doesNotContain=" + DEFAULT_FOTO);

        // Get all the usuarioList where foto does not contain UPDATED_FOTO
        defaultUsuarioShouldBeFound("foto.doesNotContain=" + UPDATED_FOTO);
    }


    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId equals to DEFAULT_USER_ID
        defaultUsuarioShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the usuarioList where userId equals to UPDATED_USER_ID
        defaultUsuarioShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId not equals to DEFAULT_USER_ID
        defaultUsuarioShouldNotBeFound("userId.notEquals=" + DEFAULT_USER_ID);

        // Get all the usuarioList where userId not equals to UPDATED_USER_ID
        defaultUsuarioShouldBeFound("userId.notEquals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultUsuarioShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the usuarioList where userId equals to UPDATED_USER_ID
        defaultUsuarioShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId is not null
        defaultUsuarioShouldBeFound("userId.specified=true");

        // Get all the usuarioList where userId is null
        defaultUsuarioShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId is greater than or equal to DEFAULT_USER_ID
        defaultUsuarioShouldBeFound("userId.greaterThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the usuarioList where userId is greater than or equal to UPDATED_USER_ID
        defaultUsuarioShouldNotBeFound("userId.greaterThanOrEqual=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId is less than or equal to DEFAULT_USER_ID
        defaultUsuarioShouldBeFound("userId.lessThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the usuarioList where userId is less than or equal to SMALLER_USER_ID
        defaultUsuarioShouldNotBeFound("userId.lessThanOrEqual=" + SMALLER_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsLessThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId is less than DEFAULT_USER_ID
        defaultUsuarioShouldNotBeFound("userId.lessThan=" + DEFAULT_USER_ID);

        // Get all the usuarioList where userId is less than UPDATED_USER_ID
        defaultUsuarioShouldBeFound("userId.lessThan=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUsuariosByUserIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where userId is greater than DEFAULT_USER_ID
        defaultUsuarioShouldNotBeFound("userId.greaterThan=" + DEFAULT_USER_ID);

        // Get all the usuarioList where userId is greater than SMALLER_USER_ID
        defaultUsuarioShouldBeFound("userId.greaterThan=" + SMALLER_USER_ID);
    }


    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento equals to DEFAULT_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.equals=" + DEFAULT_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento equals to UPDATED_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.equals=" + UPDATED_TIPO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento not equals to DEFAULT_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.notEquals=" + DEFAULT_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento not equals to UPDATED_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.notEquals=" + UPDATED_TIPO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento in DEFAULT_TIPO_DOCUMENTO or UPDATED_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.in=" + DEFAULT_TIPO_DOCUMENTO + "," + UPDATED_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento equals to UPDATED_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.in=" + UPDATED_TIPO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento is not null
        defaultUsuarioShouldBeFound("tipoDocumento.specified=true");

        // Get all the usuarioList where tipoDocumento is null
        defaultUsuarioShouldNotBeFound("tipoDocumento.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento is greater than or equal to DEFAULT_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.greaterThanOrEqual=" + DEFAULT_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento is greater than or equal to UPDATED_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.greaterThanOrEqual=" + UPDATED_TIPO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento is less than or equal to DEFAULT_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.lessThanOrEqual=" + DEFAULT_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento is less than or equal to SMALLER_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.lessThanOrEqual=" + SMALLER_TIPO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsLessThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento is less than DEFAULT_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.lessThan=" + DEFAULT_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento is less than UPDATED_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.lessThan=" + UPDATED_TIPO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTipoDocumentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where tipoDocumento is greater than DEFAULT_TIPO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("tipoDocumento.greaterThan=" + DEFAULT_TIPO_DOCUMENTO);

        // Get all the usuarioList where tipoDocumento is greater than SMALLER_TIPO_DOCUMENTO
        defaultUsuarioShouldBeFound("tipoDocumento.greaterThan=" + SMALLER_TIPO_DOCUMENTO);
    }


    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento equals to DEFAULT_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.equals=" + DEFAULT_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento equals to UPDATED_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.equals=" + UPDATED_NUMERO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento not equals to DEFAULT_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.notEquals=" + DEFAULT_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento not equals to UPDATED_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.notEquals=" + UPDATED_NUMERO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento in DEFAULT_NUMERO_DOCUMENTO or UPDATED_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.in=" + DEFAULT_NUMERO_DOCUMENTO + "," + UPDATED_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento equals to UPDATED_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.in=" + UPDATED_NUMERO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento is not null
        defaultUsuarioShouldBeFound("numeroDocumento.specified=true");

        // Get all the usuarioList where numeroDocumento is null
        defaultUsuarioShouldNotBeFound("numeroDocumento.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento is greater than or equal to DEFAULT_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.greaterThanOrEqual=" + DEFAULT_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento is greater than or equal to UPDATED_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.greaterThanOrEqual=" + UPDATED_NUMERO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento is less than or equal to DEFAULT_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.lessThanOrEqual=" + DEFAULT_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento is less than or equal to SMALLER_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.lessThanOrEqual=" + SMALLER_NUMERO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsLessThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento is less than DEFAULT_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.lessThan=" + DEFAULT_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento is less than UPDATED_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.lessThan=" + UPDATED_NUMERO_DOCUMENTO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByNumeroDocumentoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where numeroDocumento is greater than DEFAULT_NUMERO_DOCUMENTO
        defaultUsuarioShouldNotBeFound("numeroDocumento.greaterThan=" + DEFAULT_NUMERO_DOCUMENTO);

        // Get all the usuarioList where numeroDocumento is greater than SMALLER_NUMERO_DOCUMENTO
        defaultUsuarioShouldBeFound("numeroDocumento.greaterThan=" + SMALLER_NUMERO_DOCUMENTO);
    }


    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono equals to DEFAULT_TELEFONO
        defaultUsuarioShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the usuarioList where telefono equals to UPDATED_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono not equals to DEFAULT_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.notEquals=" + DEFAULT_TELEFONO);

        // Get all the usuarioList where telefono not equals to UPDATED_TELEFONO
        defaultUsuarioShouldBeFound("telefono.notEquals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultUsuarioShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the usuarioList where telefono equals to UPDATED_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono is not null
        defaultUsuarioShouldBeFound("telefono.specified=true");

        // Get all the usuarioList where telefono is null
        defaultUsuarioShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono is greater than or equal to DEFAULT_TELEFONO
        defaultUsuarioShouldBeFound("telefono.greaterThanOrEqual=" + DEFAULT_TELEFONO);

        // Get all the usuarioList where telefono is greater than or equal to UPDATED_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.greaterThanOrEqual=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono is less than or equal to DEFAULT_TELEFONO
        defaultUsuarioShouldBeFound("telefono.lessThanOrEqual=" + DEFAULT_TELEFONO);

        // Get all the usuarioList where telefono is less than or equal to SMALLER_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.lessThanOrEqual=" + SMALLER_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsLessThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono is less than DEFAULT_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.lessThan=" + DEFAULT_TELEFONO);

        // Get all the usuarioList where telefono is less than UPDATED_TELEFONO
        defaultUsuarioShouldBeFound("telefono.lessThan=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllUsuariosByTelefonoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList where telefono is greater than DEFAULT_TELEFONO
        defaultUsuarioShouldNotBeFound("telefono.greaterThan=" + DEFAULT_TELEFONO);

        // Get all the usuarioList where telefono is greater than SMALLER_TELEFONO
        defaultUsuarioShouldBeFound("telefono.greaterThan=" + SMALLER_TELEFONO);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUsuarioShouldBeFound(String filter) throws Exception {
        restUsuarioMockMvc.perform(get("/api/usuarios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].loginUsuario").value(hasItem(DEFAULT_LOGIN_USUARIO)))
            .andExpect(jsonPath("$.[*].nombreUsuario").value(hasItem(DEFAULT_NOMBRE_USUARIO)))
            .andExpect(jsonPath("$.[*].foto").value(hasItem(DEFAULT_FOTO)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].tipoDocumento").value(hasItem(DEFAULT_TIPO_DOCUMENTO.intValue())))
            .andExpect(jsonPath("$.[*].numeroDocumento").value(hasItem(DEFAULT_NUMERO_DOCUMENTO.intValue())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.intValue())));

        // Check, that the count call also returns 1
        restUsuarioMockMvc.perform(get("/api/usuarios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUsuarioShouldNotBeFound(String filter) throws Exception {
        restUsuarioMockMvc.perform(get("/api/usuarios?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUsuarioMockMvc.perform(get("/api/usuarios/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingUsuario() throws Exception {
        // Get the usuario
        restUsuarioMockMvc.perform(get("/api/usuarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        int databaseSizeBeforeUpdate = usuarioRepository.findAll().size();

        // Update the usuario
        Usuario updatedUsuario = usuarioRepository.findById(usuario.getId()).get();
        // Disconnect from session so that the updates on updatedUsuario are not directly saved in db
        em.detach(updatedUsuario);
        updatedUsuario
            .loginUsuario(UPDATED_LOGIN_USUARIO)
            .nombreUsuario(UPDATED_NOMBRE_USUARIO)
            .foto(UPDATED_FOTO)
            .userId(UPDATED_USER_ID)
            .tipoDocumento(UPDATED_TIPO_DOCUMENTO)
            .numeroDocumento(UPDATED_NUMERO_DOCUMENTO)
            .telefono(UPDATED_TELEFONO);
        UsuarioDTO usuarioDTO = usuarioMapper.toDto(updatedUsuario);

        restUsuarioMockMvc.perform(put("/api/usuarios")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuarioDTO)))
            .andExpect(status().isOk());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeUpdate);
        Usuario testUsuario = usuarioList.get(usuarioList.size() - 1);
        assertThat(testUsuario.getLoginUsuario()).isEqualTo(UPDATED_LOGIN_USUARIO);
        assertThat(testUsuario.getNombreUsuario()).isEqualTo(UPDATED_NOMBRE_USUARIO);
        assertThat(testUsuario.getFoto()).isEqualTo(UPDATED_FOTO);
        assertThat(testUsuario.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testUsuario.getTipoDocumento()).isEqualTo(UPDATED_TIPO_DOCUMENTO);
        assertThat(testUsuario.getNumeroDocumento()).isEqualTo(UPDATED_NUMERO_DOCUMENTO);
        assertThat(testUsuario.getTelefono()).isEqualTo(UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void updateNonExistingUsuario() throws Exception {
        int databaseSizeBeforeUpdate = usuarioRepository.findAll().size();

        // Create the Usuario
        UsuarioDTO usuarioDTO = usuarioMapper.toDto(usuario);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUsuarioMockMvc.perform(put("/api/usuarios")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(usuarioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        int databaseSizeBeforeDelete = usuarioRepository.findAll().size();

        // Delete the usuario
        restUsuarioMockMvc.perform(delete("/api/usuarios/{id}", usuario.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
