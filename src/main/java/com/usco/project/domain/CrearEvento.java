package com.usco.project.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A CrearEvento.
 */
@Entity
@Table(name = "crear_evento")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CrearEvento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nombre_evento")
    private String nombreEvento;

    @Column(name = "informacion")
    private String informacion;

    @Column(name = "lugar")
    private String lugar;

    @Column(name = "fecha_hora")
    private Instant fechaHora;

    @Column(name = "usuario_id")
    private Long usuarioId;

    @Column(name = "tipo_evento")
    private Boolean tipoEvento;

    @Column(name = "cantiad_boletas")
    private Long cantiadBoletas;

    @Column(name = "categoria_evento")
    private String categoriaEvento;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public CrearEvento nombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
        return this;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public String getInformacion() {
        return informacion;
    }

    public CrearEvento informacion(String informacion) {
        this.informacion = informacion;
        return this;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public String getLugar() {
        return lugar;
    }

    public CrearEvento lugar(String lugar) {
        this.lugar = lugar;
        return this;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Instant getFechaHora() {
        return fechaHora;
    }

    public CrearEvento fechaHora(Instant fechaHora) {
        this.fechaHora = fechaHora;
        return this;
    }

    public void setFechaHora(Instant fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public CrearEvento usuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
        return this;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Boolean isTipoEvento() {
        return tipoEvento;
    }

    public CrearEvento tipoEvento(Boolean tipoEvento) {
        this.tipoEvento = tipoEvento;
        return this;
    }

    public void setTipoEvento(Boolean tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public Long getCantiadBoletas() {
        return cantiadBoletas;
    }

    public CrearEvento cantiadBoletas(Long cantiadBoletas) {
        this.cantiadBoletas = cantiadBoletas;
        return this;
    }

    public void setCantiadBoletas(Long cantiadBoletas) {
        this.cantiadBoletas = cantiadBoletas;
    }

    public String getCategoriaEvento() {
        return categoriaEvento;
    }

    public CrearEvento categoriaEvento(String categoriaEvento) {
        this.categoriaEvento = categoriaEvento;
        return this;
    }

    public void setCategoriaEvento(String categoriaEvento) {
        this.categoriaEvento = categoriaEvento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CrearEvento)) {
            return false;
        }
        return id != null && id.equals(((CrearEvento) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CrearEvento{" +
            "id=" + getId() +
            ", nombreEvento='" + getNombreEvento() + "'" +
            ", informacion='" + getInformacion() + "'" +
            ", lugar='" + getLugar() + "'" +
            ", fechaHora='" + getFechaHora() + "'" +
            ", usuarioId=" + getUsuarioId() +
            ", tipoEvento='" + isTipoEvento() + "'" +
            ", cantiadBoletas=" + getCantiadBoletas() +
            ", categoriaEvento='" + getCategoriaEvento() + "'" +
            "}";
    }
}
