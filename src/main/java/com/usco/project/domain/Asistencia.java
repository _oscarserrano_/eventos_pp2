package com.usco.project.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Asistencia.
 */
@Entity
@Table(name = "asistencia")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Asistencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "metodo_pago")
    private Long metodoPago;

    @Column(name = "usuario_id")
    private Long usuarioId;

    @Column(name = "evento_id")
    private Long eventoId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetodoPago() {
        return metodoPago;
    }

    public Asistencia metodoPago(Long metodoPago) {
        this.metodoPago = metodoPago;
        return this;
    }

    public void setMetodoPago(Long metodoPago) {
        this.metodoPago = metodoPago;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public Asistencia usuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
        return this;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getEventoId() {
        return eventoId;
    }

    public Asistencia eventoId(Long eventoId) {
        this.eventoId = eventoId;
        return this;
    }

    public void setEventoId(Long eventoId) {
        this.eventoId = eventoId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asistencia)) {
            return false;
        }
        return id != null && id.equals(((Asistencia) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Asistencia{" +
            "id=" + getId() +
            ", metodoPago=" + getMetodoPago() +
            ", usuarioId=" + getUsuarioId() +
            ", eventoId=" + getEventoId() +
            "}";
    }
}
