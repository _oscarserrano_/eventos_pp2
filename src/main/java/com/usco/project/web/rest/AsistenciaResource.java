package com.usco.project.web.rest;

import com.usco.project.service.AsistenciaService;
import com.usco.project.web.rest.errors.BadRequestAlertException;
import com.usco.project.service.dto.AsistenciaDTO;
import com.usco.project.service.dto.AsistenciaCriteria;
import com.usco.project.service.AsistenciaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.usco.project.domain.Asistencia}.
 */
@RestController
@RequestMapping("/api")
public class AsistenciaResource {

    private final Logger log = LoggerFactory.getLogger(AsistenciaResource.class);

    private static final String ENTITY_NAME = "asistencia";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AsistenciaService asistenciaService;

    private final AsistenciaQueryService asistenciaQueryService;

    public AsistenciaResource(AsistenciaService asistenciaService, AsistenciaQueryService asistenciaQueryService) {
        this.asistenciaService = asistenciaService;
        this.asistenciaQueryService = asistenciaQueryService;
    }

    /**
     * {@code POST  /asistencias} : Create a new asistencia.
     *
     * @param asistenciaDTO the asistenciaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new asistenciaDTO, or with status {@code 400 (Bad Request)} if the asistencia has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/asistencias")
    public ResponseEntity<AsistenciaDTO> createAsistencia(@RequestBody AsistenciaDTO asistenciaDTO) throws URISyntaxException {
        log.debug("REST request to save Asistencia : {}", asistenciaDTO);
        if (asistenciaDTO.getId() != null) {
            throw new BadRequestAlertException("A new asistencia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AsistenciaDTO result = asistenciaService.save(asistenciaDTO);
        return ResponseEntity.created(new URI("/api/asistencias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /asistencias} : Updates an existing asistencia.
     *
     * @param asistenciaDTO the asistenciaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated asistenciaDTO,
     * or with status {@code 400 (Bad Request)} if the asistenciaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the asistenciaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/asistencias")
    public ResponseEntity<AsistenciaDTO> updateAsistencia(@RequestBody AsistenciaDTO asistenciaDTO) throws URISyntaxException {
        log.debug("REST request to update Asistencia : {}", asistenciaDTO);
        if (asistenciaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AsistenciaDTO result = asistenciaService.save(asistenciaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, asistenciaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /asistencias} : get all the asistencias.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of asistencias in body.
     */
    @GetMapping("/asistencias")
    public ResponseEntity<List<AsistenciaDTO>> getAllAsistencias(AsistenciaCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Asistencias by criteria: {}", criteria);
        Page<AsistenciaDTO> page = asistenciaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /asistencias/count} : count all the asistencias.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/asistencias/count")
    public ResponseEntity<Long> countAsistencias(AsistenciaCriteria criteria) {
        log.debug("REST request to count Asistencias by criteria: {}", criteria);
        return ResponseEntity.ok().body(asistenciaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /asistencias/:id} : get the "id" asistencia.
     *
     * @param id the id of the asistenciaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the asistenciaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/asistencias/{id}")
    public ResponseEntity<AsistenciaDTO> getAsistencia(@PathVariable Long id) {
        log.debug("REST request to get Asistencia : {}", id);
        Optional<AsistenciaDTO> asistenciaDTO = asistenciaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(asistenciaDTO);
    }

    /**
     * {@code DELETE  /asistencias/:id} : delete the "id" asistencia.
     *
     * @param id the id of the asistenciaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/asistencias/{id}")
    public ResponseEntity<Void> deleteAsistencia(@PathVariable Long id) {
        log.debug("REST request to delete Asistencia : {}", id);
        asistenciaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
