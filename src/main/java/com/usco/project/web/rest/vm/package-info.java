/**
 * View Models used by Spring MVC REST controllers.
 */
package com.usco.project.web.rest.vm;
