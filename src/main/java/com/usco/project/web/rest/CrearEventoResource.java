package com.usco.project.web.rest;

import com.usco.project.service.CrearEventoService;
import com.usco.project.web.rest.errors.BadRequestAlertException;
import com.usco.project.service.dto.CrearEventoDTO;
import com.usco.project.service.dto.CrearEventoCriteria;
import com.usco.project.service.CrearEventoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.usco.project.domain.CrearEvento}.
 */
@RestController
@RequestMapping("/api")
public class CrearEventoResource {

    private final Logger log = LoggerFactory.getLogger(CrearEventoResource.class);

    private static final String ENTITY_NAME = "crearEvento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CrearEventoService crearEventoService;

    private final CrearEventoQueryService crearEventoQueryService;

    public CrearEventoResource(CrearEventoService crearEventoService, CrearEventoQueryService crearEventoQueryService) {
        this.crearEventoService = crearEventoService;
        this.crearEventoQueryService = crearEventoQueryService;
    }

    /**
     * {@code POST  /crear-eventos} : Create a new crearEvento.
     *
     * @param crearEventoDTO the crearEventoDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new crearEventoDTO, or with status {@code 400 (Bad Request)} if the crearEvento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/crear-eventos")
    public ResponseEntity<CrearEventoDTO> createCrearEvento(@RequestBody CrearEventoDTO crearEventoDTO) throws URISyntaxException {
        log.debug("REST request to save CrearEvento : {}", crearEventoDTO);
        if (crearEventoDTO.getId() != null) {
            throw new BadRequestAlertException("A new crearEvento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CrearEventoDTO result = crearEventoService.save(crearEventoDTO);
        return ResponseEntity.created(new URI("/api/crear-eventos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /crear-eventos} : Updates an existing crearEvento.
     *
     * @param crearEventoDTO the crearEventoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated crearEventoDTO,
     * or with status {@code 400 (Bad Request)} if the crearEventoDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the crearEventoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/crear-eventos")
    public ResponseEntity<CrearEventoDTO> updateCrearEvento(@RequestBody CrearEventoDTO crearEventoDTO) throws URISyntaxException {
        log.debug("REST request to update CrearEvento : {}", crearEventoDTO);
        if (crearEventoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CrearEventoDTO result = crearEventoService.save(crearEventoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, crearEventoDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /crear-eventos} : get all the crearEventos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of crearEventos in body.
     */
    @GetMapping("/crear-eventos")
    public ResponseEntity<List<CrearEventoDTO>> getAllCrearEventos(CrearEventoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CrearEventos by criteria: {}", criteria);
        Page<CrearEventoDTO> page = crearEventoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /crear-eventos/count} : count all the crearEventos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/crear-eventos/count")
    public ResponseEntity<Long> countCrearEventos(CrearEventoCriteria criteria) {
        log.debug("REST request to count CrearEventos by criteria: {}", criteria);
        return ResponseEntity.ok().body(crearEventoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /crear-eventos/:id} : get the "id" crearEvento.
     *
     * @param id the id of the crearEventoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the crearEventoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/crear-eventos/{id}")
    public ResponseEntity<CrearEventoDTO> getCrearEvento(@PathVariable Long id) {
        log.debug("REST request to get CrearEvento : {}", id);
        Optional<CrearEventoDTO> crearEventoDTO = crearEventoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(crearEventoDTO);
    }

    /**
     * {@code DELETE  /crear-eventos/:id} : delete the "id" crearEvento.
     *
     * @param id the id of the crearEventoDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/crear-eventos/{id}")
    public ResponseEntity<Void> deleteCrearEvento(@PathVariable Long id) {
        log.debug("REST request to delete CrearEvento : {}", id);
        crearEventoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
