package com.usco.project.service;

import com.usco.project.service.dto.AsistenciaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.usco.project.domain.Asistencia}.
 */
public interface AsistenciaService {

    /**
     * Save a asistencia.
     *
     * @param asistenciaDTO the entity to save.
     * @return the persisted entity.
     */
    AsistenciaDTO save(AsistenciaDTO asistenciaDTO);

    /**
     * Get all the asistencias.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AsistenciaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" asistencia.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AsistenciaDTO> findOne(Long id);

    /**
     * Delete the "id" asistencia.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
