package com.usco.project.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.usco.project.domain.Usuario} entity. This class is used
 * in {@link com.usco.project.web.rest.UsuarioResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /usuarios?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UsuarioCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter loginUsuario;

    private StringFilter nombreUsuario;

    private StringFilter foto;

    private LongFilter userId;

    private LongFilter tipoDocumento;

    private LongFilter numeroDocumento;

    private LongFilter telefono;

    public UsuarioCriteria() {
    }

    public UsuarioCriteria(UsuarioCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.loginUsuario = other.loginUsuario == null ? null : other.loginUsuario.copy();
        this.nombreUsuario = other.nombreUsuario == null ? null : other.nombreUsuario.copy();
        this.foto = other.foto == null ? null : other.foto.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.tipoDocumento = other.tipoDocumento == null ? null : other.tipoDocumento.copy();
        this.numeroDocumento = other.numeroDocumento == null ? null : other.numeroDocumento.copy();
        this.telefono = other.telefono == null ? null : other.telefono.copy();
    }

    @Override
    public UsuarioCriteria copy() {
        return new UsuarioCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLoginUsuario() {
        return loginUsuario;
    }

    public void setLoginUsuario(StringFilter loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

    public StringFilter getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(StringFilter nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public StringFilter getFoto() {
        return foto;
    }

    public void setFoto(StringFilter foto) {
        this.foto = foto;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(LongFilter tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public LongFilter getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(LongFilter numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public LongFilter getTelefono() {
        return telefono;
    }

    public void setTelefono(LongFilter telefono) {
        this.telefono = telefono;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UsuarioCriteria that = (UsuarioCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(loginUsuario, that.loginUsuario) &&
            Objects.equals(nombreUsuario, that.nombreUsuario) &&
            Objects.equals(foto, that.foto) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(tipoDocumento, that.tipoDocumento) &&
            Objects.equals(numeroDocumento, that.numeroDocumento) &&
            Objects.equals(telefono, that.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        loginUsuario,
        nombreUsuario,
        foto,
        userId,
        tipoDocumento,
        numeroDocumento,
        telefono
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UsuarioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (loginUsuario != null ? "loginUsuario=" + loginUsuario + ", " : "") +
                (nombreUsuario != null ? "nombreUsuario=" + nombreUsuario + ", " : "") +
                (foto != null ? "foto=" + foto + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (tipoDocumento != null ? "tipoDocumento=" + tipoDocumento + ", " : "") +
                (numeroDocumento != null ? "numeroDocumento=" + numeroDocumento + ", " : "") +
                (telefono != null ? "telefono=" + telefono + ", " : "") +
            "}";
    }

}
