package com.usco.project.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.usco.project.domain.Asistencia} entity. This class is used
 * in {@link com.usco.project.web.rest.AsistenciaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /asistencias?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AsistenciaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter metodoPago;

    private LongFilter usuarioId;

    private LongFilter eventoId;

    public AsistenciaCriteria() {
    }

    public AsistenciaCriteria(AsistenciaCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.metodoPago = other.metodoPago == null ? null : other.metodoPago.copy();
        this.usuarioId = other.usuarioId == null ? null : other.usuarioId.copy();
        this.eventoId = other.eventoId == null ? null : other.eventoId.copy();
    }

    @Override
    public AsistenciaCriteria copy() {
        return new AsistenciaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(LongFilter metodoPago) {
        this.metodoPago = metodoPago;
    }

    public LongFilter getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(LongFilter usuarioId) {
        this.usuarioId = usuarioId;
    }

    public LongFilter getEventoId() {
        return eventoId;
    }

    public void setEventoId(LongFilter eventoId) {
        this.eventoId = eventoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AsistenciaCriteria that = (AsistenciaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(metodoPago, that.metodoPago) &&
            Objects.equals(usuarioId, that.usuarioId) &&
            Objects.equals(eventoId, that.eventoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        metodoPago,
        usuarioId,
        eventoId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AsistenciaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (metodoPago != null ? "metodoPago=" + metodoPago + ", " : "") +
                (usuarioId != null ? "usuarioId=" + usuarioId + ", " : "") +
                (eventoId != null ? "eventoId=" + eventoId + ", " : "") +
            "}";
    }

}
