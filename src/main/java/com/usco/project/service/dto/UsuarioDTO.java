package com.usco.project.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.usco.project.domain.Usuario} entity.
 */
public class UsuarioDTO implements Serializable {
    
    private Long id;

    private String loginUsuario;

    private String nombreUsuario;

    private String foto;

    private Long userId;

    private Long tipoDocumento;

    private Long numeroDocumento;

    private Long telefono;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginUsuario() {
        return loginUsuario;
    }

    public void setLoginUsuario(String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Long tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UsuarioDTO)) {
            return false;
        }

        return id != null && id.equals(((UsuarioDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UsuarioDTO{" +
            "id=" + getId() +
            ", loginUsuario='" + getLoginUsuario() + "'" +
            ", nombreUsuario='" + getNombreUsuario() + "'" +
            ", foto='" + getFoto() + "'" +
            ", userId=" + getUserId() +
            ", tipoDocumento=" + getTipoDocumento() +
            ", numeroDocumento=" + getNumeroDocumento() +
            ", telefono=" + getTelefono() +
            "}";
    }
}
