package com.usco.project.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link com.usco.project.domain.CrearEvento} entity.
 */
public class CrearEventoDTO implements Serializable {
    
    private Long id;

    private String nombreEvento;

    private String informacion;

    private String lugar;

    private Instant fechaHora;

    private Long usuarioId;

    private Boolean tipoEvento;

    private Long cantiadBoletas;

    private String categoriaEvento;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Instant getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Instant fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Boolean isTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(Boolean tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public Long getCantiadBoletas() {
        return cantiadBoletas;
    }

    public void setCantiadBoletas(Long cantiadBoletas) {
        this.cantiadBoletas = cantiadBoletas;
    }

    public String getCategoriaEvento() {
        return categoriaEvento;
    }

    public void setCategoriaEvento(String categoriaEvento) {
        this.categoriaEvento = categoriaEvento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CrearEventoDTO)) {
            return false;
        }

        return id != null && id.equals(((CrearEventoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CrearEventoDTO{" +
            "id=" + getId() +
            ", nombreEvento='" + getNombreEvento() + "'" +
            ", informacion='" + getInformacion() + "'" +
            ", lugar='" + getLugar() + "'" +
            ", fechaHora='" + getFechaHora() + "'" +
            ", usuarioId=" + getUsuarioId() +
            ", tipoEvento='" + isTipoEvento() + "'" +
            ", cantiadBoletas=" + getCantiadBoletas() +
            ", categoriaEvento='" + getCategoriaEvento() + "'" +
            "}";
    }
}
