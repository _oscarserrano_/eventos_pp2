package com.usco.project.service;

import com.usco.project.service.dto.CrearEventoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.usco.project.domain.CrearEvento}.
 */
public interface CrearEventoService {

    /**
     * Save a crearEvento.
     *
     * @param crearEventoDTO the entity to save.
     * @return the persisted entity.
     */
    CrearEventoDTO save(CrearEventoDTO crearEventoDTO);

    /**
     * Get all the crearEventos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CrearEventoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" crearEvento.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CrearEventoDTO> findOne(Long id);

    /**
     * Delete the "id" crearEvento.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
