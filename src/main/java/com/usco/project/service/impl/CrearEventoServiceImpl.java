package com.usco.project.service.impl;

import com.usco.project.service.CrearEventoService;
import com.usco.project.domain.CrearEvento;
import com.usco.project.repository.CrearEventoRepository;
import com.usco.project.service.dto.CrearEventoDTO;
import com.usco.project.service.mapper.CrearEventoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CrearEvento}.
 */
@Service
@Transactional
public class CrearEventoServiceImpl implements CrearEventoService {

    private final Logger log = LoggerFactory.getLogger(CrearEventoServiceImpl.class);

    private final CrearEventoRepository crearEventoRepository;

    private final CrearEventoMapper crearEventoMapper;

    public CrearEventoServiceImpl(CrearEventoRepository crearEventoRepository, CrearEventoMapper crearEventoMapper) {
        this.crearEventoRepository = crearEventoRepository;
        this.crearEventoMapper = crearEventoMapper;
    }

    @Override
    public CrearEventoDTO save(CrearEventoDTO crearEventoDTO) {
        log.debug("Request to save CrearEvento : {}", crearEventoDTO);
        CrearEvento crearEvento = crearEventoMapper.toEntity(crearEventoDTO);
        crearEvento = crearEventoRepository.save(crearEvento);
        return crearEventoMapper.toDto(crearEvento);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CrearEventoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CrearEventos");
        return crearEventoRepository.findAll(pageable)
            .map(crearEventoMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CrearEventoDTO> findOne(Long id) {
        log.debug("Request to get CrearEvento : {}", id);
        return crearEventoRepository.findById(id)
            .map(crearEventoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CrearEvento : {}", id);
        crearEventoRepository.deleteById(id);
    }
}
