package com.usco.project.service.impl;

import com.usco.project.service.AsistenciaService;
import com.usco.project.domain.Asistencia;
import com.usco.project.repository.AsistenciaRepository;
import com.usco.project.service.dto.AsistenciaDTO;
import com.usco.project.service.mapper.AsistenciaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Asistencia}.
 */
@Service
@Transactional
public class AsistenciaServiceImpl implements AsistenciaService {

    private final Logger log = LoggerFactory.getLogger(AsistenciaServiceImpl.class);

    private final AsistenciaRepository asistenciaRepository;

    private final AsistenciaMapper asistenciaMapper;

    public AsistenciaServiceImpl(AsistenciaRepository asistenciaRepository, AsistenciaMapper asistenciaMapper) {
        this.asistenciaRepository = asistenciaRepository;
        this.asistenciaMapper = asistenciaMapper;
    }

    @Override
    public AsistenciaDTO save(AsistenciaDTO asistenciaDTO) {
        log.debug("Request to save Asistencia : {}", asistenciaDTO);
        Asistencia asistencia = asistenciaMapper.toEntity(asistenciaDTO);
        asistencia = asistenciaRepository.save(asistencia);
        return asistenciaMapper.toDto(asistencia);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AsistenciaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Asistencias");
        return asistenciaRepository.findAll(pageable)
            .map(asistenciaMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AsistenciaDTO> findOne(Long id) {
        log.debug("Request to get Asistencia : {}", id);
        return asistenciaRepository.findById(id)
            .map(asistenciaMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Asistencia : {}", id);
        asistenciaRepository.deleteById(id);
    }
}
