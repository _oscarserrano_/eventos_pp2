package com.usco.project.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.usco.project.domain.Asistencia;
import com.usco.project.domain.*; // for static metamodels
import com.usco.project.repository.AsistenciaRepository;
import com.usco.project.service.dto.AsistenciaCriteria;
import com.usco.project.service.dto.AsistenciaDTO;
import com.usco.project.service.mapper.AsistenciaMapper;

/**
 * Service for executing complex queries for {@link Asistencia} entities in the database.
 * The main input is a {@link AsistenciaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AsistenciaDTO} or a {@link Page} of {@link AsistenciaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AsistenciaQueryService extends QueryService<Asistencia> {

    private final Logger log = LoggerFactory.getLogger(AsistenciaQueryService.class);

    private final AsistenciaRepository asistenciaRepository;

    private final AsistenciaMapper asistenciaMapper;

    public AsistenciaQueryService(AsistenciaRepository asistenciaRepository, AsistenciaMapper asistenciaMapper) {
        this.asistenciaRepository = asistenciaRepository;
        this.asistenciaMapper = asistenciaMapper;
    }

    /**
     * Return a {@link List} of {@link AsistenciaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AsistenciaDTO> findByCriteria(AsistenciaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Asistencia> specification = createSpecification(criteria);
        return asistenciaMapper.toDto(asistenciaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AsistenciaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AsistenciaDTO> findByCriteria(AsistenciaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Asistencia> specification = createSpecification(criteria);
        return asistenciaRepository.findAll(specification, page)
            .map(asistenciaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AsistenciaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Asistencia> specification = createSpecification(criteria);
        return asistenciaRepository.count(specification);
    }

    /**
     * Function to convert {@link AsistenciaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Asistencia> createSpecification(AsistenciaCriteria criteria) {
        Specification<Asistencia> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Asistencia_.id));
            }
            if (criteria.getMetodoPago() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMetodoPago(), Asistencia_.metodoPago));
            }
            if (criteria.getUsuarioId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUsuarioId(), Asistencia_.usuarioId));
            }
            if (criteria.getEventoId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEventoId(), Asistencia_.eventoId));
            }
        }
        return specification;
    }
}
