package com.usco.project.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.usco.project.domain.CrearEvento;
import com.usco.project.domain.*; // for static metamodels
import com.usco.project.repository.CrearEventoRepository;
import com.usco.project.service.dto.CrearEventoCriteria;
import com.usco.project.service.dto.CrearEventoDTO;
import com.usco.project.service.mapper.CrearEventoMapper;

/**
 * Service for executing complex queries for {@link CrearEvento} entities in the database.
 * The main input is a {@link CrearEventoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CrearEventoDTO} or a {@link Page} of {@link CrearEventoDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CrearEventoQueryService extends QueryService<CrearEvento> {

    private final Logger log = LoggerFactory.getLogger(CrearEventoQueryService.class);

    private final CrearEventoRepository crearEventoRepository;

    private final CrearEventoMapper crearEventoMapper;

    public CrearEventoQueryService(CrearEventoRepository crearEventoRepository, CrearEventoMapper crearEventoMapper) {
        this.crearEventoRepository = crearEventoRepository;
        this.crearEventoMapper = crearEventoMapper;
    }

    /**
     * Return a {@link List} of {@link CrearEventoDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CrearEventoDTO> findByCriteria(CrearEventoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CrearEvento> specification = createSpecification(criteria);
        return crearEventoMapper.toDto(crearEventoRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CrearEventoDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CrearEventoDTO> findByCriteria(CrearEventoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CrearEvento> specification = createSpecification(criteria);
        return crearEventoRepository.findAll(specification, page)
            .map(crearEventoMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CrearEventoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CrearEvento> specification = createSpecification(criteria);
        return crearEventoRepository.count(specification);
    }

    /**
     * Function to convert {@link CrearEventoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CrearEvento> createSpecification(CrearEventoCriteria criteria) {
        Specification<CrearEvento> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CrearEvento_.id));
            }
            if (criteria.getNombreEvento() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNombreEvento(), CrearEvento_.nombreEvento));
            }
            if (criteria.getInformacion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInformacion(), CrearEvento_.informacion));
            }
            if (criteria.getLugar() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLugar(), CrearEvento_.lugar));
            }
            if (criteria.getFechaHora() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFechaHora(), CrearEvento_.fechaHora));
            }
            if (criteria.getUsuarioId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUsuarioId(), CrearEvento_.usuarioId));
            }
            if (criteria.getTipoEvento() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoEvento(), CrearEvento_.tipoEvento));
            }
            if (criteria.getCantiadBoletas() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCantiadBoletas(), CrearEvento_.cantiadBoletas));
            }
            if (criteria.getCategoriaEvento() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoriaEvento(), CrearEvento_.categoriaEvento));
            }
        }
        return specification;
    }
}
