import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppEventosOscarSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE, MHOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { CrearEventoComponent } from './crear-evento/crear-evento.component';
import { AppEventosOscarCrearEventoModule } from '../entities/crear-evento/crear-evento.module'

@NgModule({
  imports: [AppEventosOscarSharedModule, AppEventosOscarCrearEventoModule, RouterModule.forChild([MHOME_ROUTE])],
  declarations: [HomeComponent, CrearEventoComponent],
})
export class AppEventosOscarHomeModule {}
