import { Route } from '@angular/router';

import { HomeComponent } from './home.component';

import { CrearEventoComponent } from './crear-evento/crear-evento.component';


export const HomeCrearEvento: Route = {
  path: 'crear-evento/:creaUsuario',
  component: CrearEventoComponent,
  data: {
    authorities: [],
    pageTitle: 'Crear Eventos',
  },
}

export const HOME_ROUTE: Route = {
  path: 'home',
  component: HomeComponent,
  data: {
    authorities: [],
    pageTitle: 'Eventos',
  },
};

const MHOME_ROUTES = [HOME_ROUTE, HomeCrearEvento]

export const MHOME_ROUTE: Route = {
  path: '',
  children: MHOME_ROUTES
};
