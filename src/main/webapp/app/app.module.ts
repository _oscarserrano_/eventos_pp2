import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AppEventosOscarSharedModule } from 'app/shared/shared.module';
import { AppEventosOscarCoreModule } from 'app/core/core.module';
import { AppEventosOscarAppRoutingModule } from './app-routing.module';
import { AppEventosOscarHomeModule } from './home/home.module';
import { AppEventosOscarEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    AppEventosOscarSharedModule,
    AppEventosOscarCoreModule,
    AppEventosOscarHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AppEventosOscarEntityModule,
    AppEventosOscarAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class AppEventosOscarAppModule {}
