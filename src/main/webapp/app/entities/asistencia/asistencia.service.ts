import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAsistencia } from 'app/shared/model/asistencia.model';

type EntityResponseType = HttpResponse<IAsistencia>;
type EntityArrayResponseType = HttpResponse<IAsistencia[]>;

@Injectable({ providedIn: 'root' })
export class AsistenciaService {
  public resourceUrl = SERVER_API_URL + 'api/asistencias';

  constructor(protected http: HttpClient) {}

  create(asistencia: IAsistencia): Observable<EntityResponseType> {
    return this.http.post<IAsistencia>(this.resourceUrl, asistencia, { observe: 'response' });
  }

  update(asistencia: IAsistencia): Observable<EntityResponseType> {
    return this.http.put<IAsistencia>(this.resourceUrl, asistencia, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAsistencia>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAsistencia[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
