import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAsistencia, Asistencia } from 'app/shared/model/asistencia.model';
import { AsistenciaService } from './asistencia.service';
import { AsistenciaComponent } from './asistencia.component';
import { AsistenciaDetailComponent } from './asistencia-detail.component';
import { AsistenciaUpdateComponent } from './asistencia-update.component';

@Injectable({ providedIn: 'root' })
export class AsistenciaResolve implements Resolve<IAsistencia> {
  constructor(private service: AsistenciaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAsistencia> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((asistencia: HttpResponse<Asistencia>) => {
          if (asistencia.body) {
            return of(asistencia.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Asistencia());
  }
}

export const asistenciaRoute: Routes = [
  {
    path: '',
    component: AsistenciaComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Asistencias',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AsistenciaDetailComponent,
    resolve: {
      asistencia: AsistenciaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Asistencias',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AsistenciaUpdateComponent,
    resolve: {
      asistencia: AsistenciaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Asistencias',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AsistenciaUpdateComponent,
    resolve: {
      asistencia: AsistenciaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Asistencias',
    },
    canActivate: [UserRouteAccessService],
  },
];
