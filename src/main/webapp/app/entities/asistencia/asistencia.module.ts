import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppEventosOscarSharedModule } from 'app/shared/shared.module';
import { AsistenciaComponent } from './asistencia.component';
import { AsistenciaDetailComponent } from './asistencia-detail.component';
import { AsistenciaUpdateComponent } from './asistencia-update.component';
import { AsistenciaDeleteDialogComponent } from './asistencia-delete-dialog.component';
import { asistenciaRoute } from './asistencia.route';

@NgModule({
  imports: [AppEventosOscarSharedModule, RouterModule.forChild(asistenciaRoute)],
  declarations: [AsistenciaComponent, AsistenciaDetailComponent, AsistenciaUpdateComponent, AsistenciaDeleteDialogComponent],
  entryComponents: [AsistenciaDeleteDialogComponent],
})
export class AppEventosOscarAsistenciaModule {}
