import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAsistencia, Asistencia } from 'app/shared/model/asistencia.model';
import { AsistenciaService } from './asistencia.service';

@Component({
  selector: 'jhi-asistencia-update',
  templateUrl: './asistencia-update.component.html',
})
export class AsistenciaUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    metodoPago: [],
    usuarioId: [],
    eventoId: [],
  });

  constructor(protected asistenciaService: AsistenciaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ asistencia }) => {
      this.updateForm(asistencia);
    });
  }

  updateForm(asistencia: IAsistencia): void {
    this.editForm.patchValue({
      id: asistencia.id,
      metodoPago: asistencia.metodoPago,
      usuarioId: asistencia.usuarioId,
      eventoId: asistencia.eventoId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const asistencia = this.createFromForm();
    if (asistencia.id !== undefined) {
      this.subscribeToSaveResponse(this.asistenciaService.update(asistencia));
    } else {
      this.subscribeToSaveResponse(this.asistenciaService.create(asistencia));
    }
  }

  private createFromForm(): IAsistencia {
    return {
      ...new Asistencia(),
      id: this.editForm.get(['id'])!.value,
      metodoPago: this.editForm.get(['metodoPago'])!.value,
      usuarioId: this.editForm.get(['usuarioId'])!.value,
      eventoId: this.editForm.get(['eventoId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAsistencia>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
