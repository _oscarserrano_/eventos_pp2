import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICrearEvento } from 'app/shared/model/crear-evento.model';

@Component({
  selector: 'jhi-crear-evento-detail',
  templateUrl: './crear-evento-detail.component.html',
})
export class CrearEventoDetailComponent implements OnInit {
  crearEvento: ICrearEvento | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ crearEvento }) => (this.crearEvento = crearEvento));
  }

  previousState(): void {
    window.history.back();
  }
}
