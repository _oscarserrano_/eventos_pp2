import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICrearEvento, CrearEvento } from 'app/shared/model/crear-evento.model';
import { CrearEventoService } from './crear-evento.service';
import { CrearEventoComponent } from './crear-evento.component';
import { CrearEventoDetailComponent } from './crear-evento-detail.component';
import { CrearEventoUpdateComponent } from './crear-evento-update.component';

@Injectable({ providedIn: 'root' })
export class CrearEventoResolve implements Resolve<ICrearEvento> {
  constructor(private service: CrearEventoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICrearEvento> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((crearEvento: HttpResponse<CrearEvento>) => {
          if (crearEvento.body) {
            return of(crearEvento.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CrearEvento());
  }
}

export const crearEventoRoute: Routes = [
  {
    path: '',
    component: CrearEventoComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CrearEventos',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CrearEventoDetailComponent,
    resolve: {
      crearEvento: CrearEventoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CrearEventos',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CrearEventoUpdateComponent,
    resolve: {
      crearEvento: CrearEventoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CrearEventos',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CrearEventoUpdateComponent,
    resolve: {
      crearEvento: CrearEventoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CrearEventos',
    },
    canActivate: [UserRouteAccessService],
  },
];
