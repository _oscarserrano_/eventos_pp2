import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICrearEvento } from 'app/shared/model/crear-evento.model';

type EntityResponseType = HttpResponse<ICrearEvento>;
type EntityArrayResponseType = HttpResponse<ICrearEvento[]>;

@Injectable({ providedIn: 'root' })
export class CrearEventoService {
  public resourceUrl = SERVER_API_URL + 'api/crear-eventos';

  constructor(protected http: HttpClient) {}

  create(crearEvento: ICrearEvento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(crearEvento);
    return this.http
      .post<ICrearEvento>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(crearEvento: ICrearEvento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(crearEvento);
    return this.http
      .put<ICrearEvento>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICrearEvento>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICrearEvento[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(crearEvento: ICrearEvento): ICrearEvento {
    const copy: ICrearEvento = Object.assign({}, crearEvento, {
      fechaHora: crearEvento.fechaHora && crearEvento.fechaHora.isValid() ? crearEvento.fechaHora.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaHora = res.body.fechaHora ? moment(res.body.fechaHora) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((crearEvento: ICrearEvento) => {
        crearEvento.fechaHora = crearEvento.fechaHora ? moment(crearEvento.fechaHora) : undefined;
      });
    }
    return res;
  }
}
