import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICrearEvento } from 'app/shared/model/crear-evento.model';
import { CrearEventoService } from './crear-evento.service';

@Component({
  templateUrl: './crear-evento-delete-dialog.component.html',
})
export class CrearEventoDeleteDialogComponent {
  crearEvento?: ICrearEvento;

  constructor(
    protected crearEventoService: CrearEventoService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.crearEventoService.delete(id).subscribe(() => {
      this.eventManager.broadcast('crearEventoListModification');
      this.activeModal.close();
    });
  }
}
