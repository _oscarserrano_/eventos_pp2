import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICrearEvento, CrearEvento } from 'app/shared/model/crear-evento.model';
import { CrearEventoService } from './crear-evento.service';

@Component({
  selector: 'jhi-crear-evento-update',
  templateUrl: './crear-evento-update.component.html',
})
export class CrearEventoUpdateComponent implements OnInit {
  isSaving = false;

  isCrearEvento = false;

  tipoEvento?:boolean;

  editForm = this.fb.group({
    id: [],
    nombreEvento: [],
    informacion: [],
    lugar: [],
    fechaHora: [],
    usuarioId: [],
    tipoEvento: [],
    cantiadBoletas: [],
    categoriaEvento: [],
  });

  constructor(protected crearEventoService: CrearEventoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      if (params['creaUsuario'] === 'true') {
        this.isCrearEvento = true;
      }
    });
    this.activatedRoute.data.subscribe(({ crearEvento }) => {
      if (!crearEvento.id) {
        const today = moment().startOf('day');
        crearEvento.fechaHora = today;
      }

      this.updateForm(crearEvento);
    });
  }

  updateForm(crearEvento: ICrearEvento): void {
    this.editForm.patchValue({
      id: crearEvento.id,
      nombreEvento: crearEvento.nombreEvento,
      informacion: crearEvento.informacion,
      lugar: crearEvento.lugar,
      fechaHora: crearEvento.fechaHora ? crearEvento.fechaHora.format(DATE_TIME_FORMAT) : null,
      usuarioId: crearEvento.usuarioId,
      tipoEvento: crearEvento.tipoEvento,
      cantiadBoletas: crearEvento.cantiadBoletas,
      categoriaEvento: crearEvento.categoriaEvento,
    });
  }

  configurarTipoEvento(isPrivado: boolean): void {
    //this.editForm.setControl['tipoEvento'].setValue(isPrivado);
    this.tipoEvento = isPrivado;
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const crearEvento = this.createFromForm();
    console.warn('id create evento: ', crearEvento);
    console.warn('id create evento: ', crearEvento.id);
    if (this.tipoEvento !== null && this.tipoEvento !== undefined) {
      crearEvento.tipoEvento = this.tipoEvento;
    }
    if (crearEvento.id !== null && crearEvento.id !== undefined) {
      this.subscribeToSaveResponse(this.crearEventoService.update(crearEvento));
    } else {
      this.subscribeToSaveResponse(this.crearEventoService.create(crearEvento));
    }
  }

  private createFromForm(): ICrearEvento {
    return {
      ...new CrearEvento(),
      id: this.editForm.get(['id'])!.value,
      nombreEvento: this.editForm.get(['nombreEvento'])!.value,
      informacion: this.editForm.get(['informacion'])!.value,
      lugar: this.editForm.get(['lugar'])!.value,
      fechaHora: this.editForm.get(['fechaHora'])!.value ? moment(this.editForm.get(['fechaHora'])!.value, DATE_TIME_FORMAT) : undefined,
      usuarioId: this.editForm.get(['usuarioId'])!.value,
      tipoEvento: this.editForm.get(['tipoEvento'])!.value,
      cantiadBoletas: this.editForm.get(['cantiadBoletas'])!.value,
      categoriaEvento: this.editForm.get(['categoriaEvento'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICrearEvento>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
