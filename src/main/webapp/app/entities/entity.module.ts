import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'crear-evento',
        loadChildren: () => import('./crear-evento/crear-evento.module').then(m => m.AppEventosOscarCrearEventoModule),
      },
      {
        path: 'usuario',
        loadChildren: () => import('./usuario/usuario.module').then(m => m.AppEventosOscarUsuarioModule),
      },
      {
        path: 'asistencia',
        loadChildren: () => import('./asistencia/asistencia.module').then(m => m.AppEventosOscarAsistenciaModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class AppEventosOscarEntityModule {}
