export interface IAsistencia {
  id?: number;
  metodoPago?: number;
  usuarioId?: number;
  eventoId?: number;
}

export class Asistencia implements IAsistencia {
  constructor(public id?: number, public metodoPago?: number, public usuarioId?: number, public eventoId?: number) {}
}
